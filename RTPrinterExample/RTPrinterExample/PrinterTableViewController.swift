//
//  PrinterTableViewController.swift
//  RTPrinterExample
//
//  Created by Paul Lee on 2021/3/3.
//

import UIKit
class PrinterTableViewController: UITableViewController {
    var bleInterface: RTBlueToothPI!
    let currentprinter = Printer()
    override func viewDidLoad() {
        super.viewDidLoad()
        registerNotification()
        bleInterface = BlueToothFactory.create(BlueToothKind(0))!
        
        bleInterface.isNeedCallBack = true

        bleInterface.callbackConnected = { [unowned self] obj in
            self.printerConnected(obj) // 连接后会正确呼叫这行
        }
        
        bleInterface.callbackDisConnect = { _ in
            print("printer disconnected") // 这行尚未实际测试过 ...
        }
        
        bleInterface.callbackPrintFinsh = { _, _ in
            print("print finished") //预期打印完会呼叫这行，但实际上不会
        }
        
        bleInterface.callbackwhenSendProgressUpdate = { _, _, _ in
            print("progress update") // 打印过程中完全不会呼叫这行
        }
        
        bleInterface.callbackwhenSendSuccess = { _, _, _ in
            print("send success") // 打印过程中完全不会呼叫这行
        }
        
        bleInterface.callbackwhenSendFailure = { _, _, _ in
            print("send failure") // 打印过程中完全不会呼叫这行
        }
    }
    
    func registerNotification() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(bleToothReady(_:)),
            name: NSNotification.Name(rawValue: BleSystemBluetoothReady),
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(foundDevice(_:)),
            name: NSNotification.Name(rawValue: BleServiceFindDevice),
            object: nil
        )
    }
    
    @objc private func bleToothReady(_ notification: Notification) {
        bleInterface.startScan(30, isclear: true)
    }
    
    private var deviceLists: [RTDeviceinfo] = []
    @objc private func foundDevice(_ notification: Notification) {
        deviceLists = bleInterface.getBleDevicelist() as? [RTDeviceinfo] ?? []
        tableView.reloadData()
        if deviceLists.map({$0.name}).contains("RPP200") {
            bleInterface.stopScan()
        }
    }
        
    private func printerConnected(_ observerObj: ObserverObj?) {
        guard let printerInterface = observerObj?.msgobj as? PrinterInterface else {
            return
        }
        
        if currentprinter.printerPi === printerInterface {
            print("printer connected")
            tableView.reloadData()
        }
    }
    
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return deviceLists.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let device = deviceLists[indexPath.row]
        
        var isConnected = false
        if let connectedUUID = currentprinter.printerPi?.address,
           connectedUUID == device.uuid {
            isConnected = true
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        cell.textLabel?.text = "name: \(device.name ?? ""), rssi: \(device.rssiLevel), connected: \(isConnected)"
        cell.detailTextLabel?.text = device.shortAddress
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let device = deviceLists[indexPath.row]
    
        guard device.name.contains("RP") else {
            fatalError()
        }
        
        doConnect(device.uuid)
    }
    
    private func doConnect(_ uuid: String) {
        bleInterface.mtuLength = 100
        bleInterface.sendDelayMS = 20
        bleInterface.address = uuid
        bleInterface.printerCmdtype = PrinterCmdESC
        currentprinter.printerPi = bleInterface
        currentprinter.open()
    }
    
    //MARK: - print image
    lazy var image: UIImage = {
        let url = Bundle.main.url(
            forResource: "pinion",
            withExtension: ".jpg"
        )!
        let data = try! Data(contentsOf: url)
        let image = UIImage(data: data)
        return image!
    }()
    
    
    
    @IBAction func btnImagePrint(_ sender: Any) {
        let ilimitwidth = 72
        let cmd = ESCFactory.create()!
        cmd.clear()
        cmd.encodingType = Encoding_UTF8
        
        //
        let headercmd = cmd.getHeaderCmd()
            
        cmd.append(headercmd)
        
        //
        let bitmapSetting = currentprinter.bitmapSetts
        bitmapSetting?.alignmode = Align_Center
        bitmapSetting?.limitWidth = ilimitwidth * 8 //ESC
        let data = cmd.getBitMapCmd(bitmapSetting, image: image)!
        cmd.append(data)
        
        //
        cmd.append(cmd.getLFCRCmd())
        cmd.append(cmd.getPrintEnd(1))
        
        if currentprinter.isOpen {
            let data = cmd.getCmd()
            currentprinter.writeAsync(data)
        }
    }
}
